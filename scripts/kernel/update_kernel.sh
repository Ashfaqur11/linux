#!/bin/bash
if [ `whoami` != root ]; then
    echo Please run this script as root or using sudo
    exit
fi
uname -r
cwd=$(pwd)
cd /tmp
echo Switched to directory:
pwd
mkdir kernel-download
cd kernel-download
echo Downloading Kernel 5.16.14
wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16.14/amd64/linux-headers-5.16.14-051614-generic_5.16.14-051614.202203111231_amd64.deb
wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16.14/amd64/linux-headers-5.16.14-051614_5.16.14-051614.202203111231_all.deb
wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16.14/amd64/linux-image-unsigned-5.16.14-051614-generic_5.16.14-051614.202203111231_amd64.deb
wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.16.14/amd64/linux-modules-5.16.14-051614-generic_5.16.14-051614.202203111231_amd64.deb
echo Installing Kernel 5.16.14
chmod +x *.deb
dpkg -i *.deb
reboot

