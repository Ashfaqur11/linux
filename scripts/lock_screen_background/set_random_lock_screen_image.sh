#!/bin/bash
THEME="landscape"
DATE=`date "+%d_%m_%Y-%H_%M_%S"`
FILE_NAME=$THEME-$DATE.jpg
WALLPAPERS_FOLDER=/home/ash/Pictures/wallpapers/
FILE_PATH=$WALLPAPERS_FOLDER$FILE_NAME
echo Setting a new lock screen background with theme \"$THEME\"
echo Saving new image with file path \"$FILE_PATH\"
wget --output-document=$FILE_PATH https://source.unsplash.com/random/3480x2160/?$THEME
sudo /home/ash/sb/linux/scripts/lock_screen_background/ubuntu-gdm-set-background --image $FILE_PATH