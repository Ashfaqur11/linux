This repository contains guides to setup Ubuntu for the first time.

- [Ubuntu Setup](#ubuntu-setup)
  - [Downloading image](#downloading-image)
  - [TIPS](#tips)
  - [Virtual Box](#virtual-box)
    - [Install virtualbox guest additions](#install-virtualbox-guest-additions)
  - [Update](#update)
  - [Auto Remove](#auto-remove)
  - [Update Kernel](#update-kernel)
  - [Unrestricted Extras](#unrestricted-extras)
  - [Install Sofware](#install-sofware)
  - [Generate SSH](#generate-ssh)
  - [Flatpak](#flatpak)
  - [JAVA](#java)
- [UI Customization](#ui-customization)
  - [D-Conf Editor](#d-conf-editor)
  - [Dock Bottom](#dock-bottom)
  - [Tweaks](#tweaks)
  - [Flatpak Extension Manager](#flatpak-extension-manager)
  - [Weather app](#weather-app)

# Ubuntu Setup

## Downloading image

Latest ubuntu image is available from official website:

https://ubuntu.com/download/desktop

## TIPS

If you need to use user proxy settings during sudo

    sudo -EH

## Virtual Box

Download virtual box if you want to try out linux in a VM

https://www.virtualbox.org/wiki/Downloads

Setup a Ubuntu VM and load the iso file from settings:

*Storage -> Optical Drive*

Allow shared clipboard:

*General -> Advanced -> Shared Clipboard -> Bidirectional*

Allow atleast 4GB RAM, 2 CPU


### Install virtualbox guest additions

This is needed for linux VM to work correctly including using clipboard copy and display settings. 

    sudo apt-get install -y virtualbox-guest-additions-iso

Then mount the iso and run.

    /usr/share/virtualbox/VBoxGuestAdditions.iso

Run from terminal if it does not run:

    ./autorun.sh

Unmount and finally reboot

    sudo reboot

Source: https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm/

## Update

Update first after installing ubuntu.

    sudo apt-get update

Upgrade

    sudo apt-get upgrade -y

## Auto Remove

Remove unnecessary stuff

    sudo apt autoremove -y


## Update Kernel

If an updated kernel version is needed, download and install from source.
Modify link in script if needed

    sudo ./scripts/kernel/update_kernel.sh

## Unrestricted Extras

Install codecs and MS fonts

    sudo apt-get install -y ubuntu-restricted-extras ubuntu-restricted-addons

## Install Sofware

Synaptic for package management

VLC for video player

GIMP for image viewer

GParted for disk management

GIT for version control

Preload daemon for efficiency

    sudo apt-get install -y gimp synaptic vlc gparted git preload

## Generate SSH

    ssh-keygen -t rsa

By default generated in home dir:

    ~/.ssh

Then copy the public key and store it in your online repository so this computer can access.

## Flatpak

    sudo apt install flatpak -y

    sudo apt install gnome-software-plugin-flatpak -y

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    
    sudo reboot

## JAVA

    sudo apt-get install -y openjdk-17-jdk


# UI Customization

## D-Conf Editor

For UI Customization

    sudo apt-get install -y dconf-editor

Setting custom columns for nautilus
    
    org/gnome/nautilus/list-view/default-visible-columns

## Dock Bottom

Move dock to bottom section

    gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top true
    gsettings set org.gnome.shell.extensions.dash-to-dock extend-height true
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
    gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
    gsettings set org.gnome.shell.extensions.dash-to-dock background-opacity 0.9
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 35
    gsettings set org.gnome.shell.extensions.dash-to-dock click-action focus-minimize-or-previews
    gsettings set org.gnome.desktop.interface cursor-size 35

## Tweaks

More UI Customizations

    sudo apt install gnome-tweaks
    
## Flatpak Extension Manager

    sudo flatpak install flathub com.mattjakeman.ExtensionManager -y

Then install User Themes from Extension Manager

## Weather app

    sudo flatpak install org.kde.kweather -y
